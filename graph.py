import networkx as nx
import matplotlib.pyplot as plt

import collections
import math
def construct_graph():
    G = nx.Graph()
    with open('adjacent.txt','r',encoding='utf-8') as rf:
        lines = rf.readlines()

    adjacent = []
    node_labels = {}
    for line in lines:
        followee = line.split(';')[:20]
        followee = [item.split(':') for item in followee]
        adjacent.append(followee)
    for followee in adjacent:
        user1 = followee[0]
        G.add_nodes_from([(int(user1[0]), {'name': user1[1]})])
        node_labels[int(user1[0])] = user1[1]
        for user in followee[1:]:
            G.add_nodes_from([(int(user[0]),{'name':user[1]})])
            G.add_edge(int(user1[0]),int(user[0]))
    return G,node_labels


def plot_network(G,node_labels):
    fig = plt.figure()
    plt.axis('off')
    # nx.draw_networkx(G,  pos=nx.spring_layout(G),node_size = 35, alpha = 0.45, width = 0.1, edge_color = 'k', with_labels = False, font_size = 6)
    nx.draw_networkx(G, pos=nx.spring_layout(G, k=2 / math.sqrt(G.order())), node_size=20, alpha=0.45, width=0.1,
                     edge_color='k', font_size=4, labels=node_labels)
    plt.savefig('network.png', dpi=600)
    plt.show()

def plot_degree_hist(G):
    degree_sequence = sorted([d for n, d in G.degree()], reverse=False)  # degree sequence
    degreeCount = collections.Counter(degree_sequence)
    deg, cnt = zip(*degreeCount.items())
    fig = plt.figure()

    plt.bar(range(len(cnt)), cnt,tick_label=deg,width=0.80, color='b')

    plt.title("Degree Histogram")
    plt.ylabel("Count")
    plt.xlabel("Degree")

    plt.savefig('degree_hist.png', dpi=600)
    plt.show()

def plot_dist(data_dict,title,filename):
    data_sort = sorted(data_dict.items(), key=lambda x: x[1], reverse=True)[:10]
    id, value = zip(*data_sort)
    plt.bar(range(len(value)), value, tick_label=id, width=0.80, color='b')
    plt.title(title)
    plt.ylabel("value")
    plt.xlabel("id")
    plt.tick_params(axis='x', labelsize=4) 
    plt.xticks(rotation=-90)
    plt.savefig(filename, dpi=600)

def plot_betweenness(G):
    bet = nx.betweenness_centrality(G)
    plot_dist(bet, title="betweenness_centrality Histogram", filename='betweenness_hist.png')
    # bet_sort = sorted(bet.items(),key=lambda x:x[1],reverse=True)[:10]
    # id,value = zip(*bet_sort)
    # plt.bar(range(len(value)), value,tick_label=id, width=0.80, color='b')
    #
    # plt.title("betweenness_centrality Histogram")
    # plt.ylabel("value")
    # plt.xlabel("id")
    #
    # plt.savefig('betweenness_hist.png', dpi=600)
    # plt.show()

def plot_clusting(G):
    c = nx.clustering(G)
    plot_dist(c, title="clusting Histogram", filename='clusting_hist.png')
    print('ok')

G,node_labels = construct_graph()
plot_network(G,node_labels)
plot_degree_hist(G)
plot_betweenness(G)
plot_clusting(G)
print('ok')


